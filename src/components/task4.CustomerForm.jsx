import React, { Component } from "react";
class CustomerForm extends Component {
  state = {
    customer: this.props.customer,
    
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    s1.customer[e.currentTarget.name] = e.currentTarget.value;
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state.customer);
  };
  render() {
    let { customer } = this.state;
    let { name, age, email } = customer;
    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={name}
              placeholder="Enter your Name"
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              id="age"
              name="age"
              value={age}
              placeholder="Enter your Age"
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control"
              id="email"
              name="email"
              value={email}
              placeholder="Enter your Email"
              onChange={this.handleChange}
            />
          </div>

          <button className="btn btn-primary m-2" onClick={this.handleSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default CustomerForm;
