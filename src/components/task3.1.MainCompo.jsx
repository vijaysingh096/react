import React, { Component } from "react";
import Task3_1CourseForm from "./task3.1.Form";
class Task3_1MainCompo extends Component {
  state = {
    courses: [],
    view: 0,
    editIndex: -1,
  };
  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };
  handleCourse = (course) => {
    let s1 = { ...this.state };
    s1.editIndex >= 0
      ? (s1.courses[s1.editIndex] = course)
      : s1.courses.push(course);
    s1.view = 0;
    s1.editIndex = -1;
    this.setState(s1);
  };
  editCourse = (index) => {
    let s1 = { ...this.state };
    s1.view = 1;
    s1.editIndex = index;
    this.setState(s1);
  };
  render() {
    let course1 = "";
    let { courses, view, editIndex } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          {view === 0 ? (
            <button
              className="btn btn-primary my-2"
              onClick={() => this.showForm()}
            >
              Add New Course{" "}
            </button>
          ) : (
            <Task3_1CourseForm
              course={editIndex >= 0 ? courses[editIndex] : course1}
              onSubmit={this.handleCourse}
              edit={editIndex >= 0}
            />
          )}
          <h3>List of Courses</h3>
          {courses.length === 0 ? (
            <p>there are ZERO Courses</p>
          ) : (
            <ul>
              {courses.map((c1, index) => (
                <li>
                  {c1}
                  <button
                    className="btn btn-warning m-2"
                    onClick={() => this.editCourse(index)}
                  >
                    Edit{" "}
                  </button>
                </li>
              ))}
            </ul>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default Task3_1MainCompo;
