import React, { Component } from "react";
import SimpleForm4 from "./task1.a.iv.SimpleForm4";
class MainComponent4 extends Component {
  state = {
    persons: [
      { name: "Jack", age: 24 },
      { name: "Mary", age: 21 },
    ],
    view: 0, //0-show person table and button,1- show form
    editPersonIndex: -1,
  };
  handlePerson = (Person) => {
    console.log("in handel person", Person);
    let s1 = { ...this.state };
    s1.editPersonIndex >= 0
      ? (s1.persons[s1.editPersonIndex] = Person)
      : s1.persons.push(Person);
    s1.editPersonIndex = -1;
    s1.view = 0;
    this.setState(s1);
  };
  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };

  editPerson = (index) => {
    let s1 = { ...this.state };
    s1.view = 1;
    s1.editPersonIndex = index;
    this.setState(s1);
  };

  render() {
    let person = { name: "", age: "" };
    let { persons, view, editPersonIndex } = this.state;
    return (
      <React.Fragment>
        {view === 0 ? (
          <div className="container">
            {persons.map((p1, index) => {
              let { name, age } = p1;
              return (
                <div className="row border">
                  <div className="col-4 border">{name}</div>
                  <div className="col-4 border">{age}</div>
                  <div className="col-4 border">
                    {" "}
                    <button
                      className="btn btn-warning btn-sm my-2"
                      onClick={() => this.editPerson(index)}
                    >
                      Edit{" "}
                    </button>{" "}
                  </div>
                </div>
              );
            })}
            <button
              className="btn btn-primary my-2"
              onClick={() => this.showForm()}
            >
              Add New Person{" "}
            </button>
          </div>
        ) : (
          <SimpleForm4
            person={editPersonIndex >= 0 ? persons[editPersonIndex] : person}
            onSubmit={this.handlePerson}
            edit={editPersonIndex >= 0}
          />
        )}
      </React.Fragment>
    );
  }
}
export default MainComponent4;
