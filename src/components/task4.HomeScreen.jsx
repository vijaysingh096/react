import React, { Component } from "react";
import CustomerForm from "./task4.CustomerForm";
class HomeScreen extends Component {
  state = {
    customers: [],
    view: 0,
    editCustomerIndex: -1,
  };

  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };
  handleCustomerList = () => {
    let s1 = { ...this.state };
    s1.view = 2;
    this.setState(s1);
  };
  handleSubmit = (customer) => {
    let s1 = { ...this.state };
    s1.editCustomerIndex >= 0
      ? (s1.customers[s1.editCustomerIndex] = customer)
      : s1.customers.push(customer);
    s1.view = 2;
    this.setState(s1);
  };
  editCustomer = (index) => {
    let s1 = { ...this.state };
    s1.view = 1;
    s1.editCustomerIndex = index;
    this.setState(s1);
  };
  render() {
    let { customers, view, editCustomerIndex } = this.state;
    let customer1 = { name: "", age: "", email: "" };
    return (
      <React.Fragment>
        <div className="container">
          <button className="btn btn-primary m-2" onClick={this.showForm}>
            New Customer
          </button>

          <button
            className="btn btn-primary m-2"
            onClick={this.handleCustomerList}
          >
            List Of Customer
          </button>
          {view === 0 ? (
            <h4>Welcome to the Customer System</h4>
          ) : view === 1 ? (
            <CustomerForm
              customer={
                editCustomerIndex >= 0
                  ? customers[editCustomerIndex]
                  : customer1
              }
              onSubmit={this.handleSubmit}
            
            />
          ) : customers.length === 0 ? (
            <h6>Ther are ZERO Customer</h6>
          ) : (
            <>
              <div className="row border bg-dark text-white">
                <div className="col-3">Name</div>
                <div className="col-3">Age</div>
                <div className="col-3">Email</div>
                <div className="col-3"></div>
              </div>
              {customers.map((c1, index) => {
                let { name, age, email } = c1;
                return (
                  <div className="row border ">
                    <div className="col-3">{name}</div>
                    <div className="col-3">{age}</div>
                    <div className="col-3">{email}</div>
                    <div className="col-3 text-center">
                      {" "}
                      <button
                        className="btn btn-success btn-sm m-2"
                        onClick={() => this.editCustomer(index)}
                      >
                        Edit{" "}
                      </button>{" "}
                    </div>
                  </div>
                );
              })}
            </>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default HomeScreen;
