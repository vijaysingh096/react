import React, { Component } from "react";
class SimpleForm2 extends Component {
  state = {
    courseDetail: { name: "", description: "", duration: "", faculty: "" },
  };

  handelChange = (e) => {
    console.log(e.currentTarget);
    let s1 = { ...this.state };
    s1.courseDetail[e.currentTarget.name] = e.currentTarget.value;
    this.setState(s1);
  };

  handelSubmit = (e) => {
    e.preventDefault();
    let { courseDetail } = this.state;
    let { name, description, duration, faculty } = courseDetail;
    console.log("handel submit ", this.state.courseDetail);
    console.log(
      "Course Details Submitted. Nmae:",
      name,
      ", Description:",
      description,
      ", Duration:",
      duration,
      "days, Faculty:",
      faculty
    );
  };

  render() {
    let { courseDetail } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h5>Course Details </h5>
          <div className="form-group">
            <label>Nmae of the Course</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={courseDetail.name}
              placeholder="Enter Name "
              onChange={this.handelChange}
            />
          </div>

          <div className="form-group">
            <label>Description</label>
            <input
              type="text"
              className="form-control"
              id="description"
              name="description"
              value={courseDetail.description}
              placeholder="Enter Course description "
              onChange={this.handelChange}
            />
          </div>

          <div className="form-group">
            <label>Duration</label>
            <input
              type="number"
              className="form-control"
              id="duration"
              name="duration"
              value={courseDetail.duration}
              placeholder="Enter Course duration "
              onChange={this.handelChange}
            />
          </div>

          <div className="form-group">
            <label>Faculty</label>
            <input
              type="text"
              className="form-control"
              id="faculty"
              name="faculty"
              value={courseDetail.faculty}
              placeholder="Enter faculty name "
              onChange={this.handelChange}
            />
          </div>

          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default SimpleForm2;
