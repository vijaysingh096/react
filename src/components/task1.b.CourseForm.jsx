import React, { Component } from "react";
class CourseForm extends Component {
  state = {
    course: this.props.course,
    edit: this.props.edit,
  };

  handelChange = (e) => {
    let s1 = { ...this.state }; 
    s1.course[e.currentTarget.name] = e.currentTarget.value;
    this.setState(s1);
};
  

  handelSubmit = (e) => {
    e.preventDefault();
    console.log("handel submit ", this.state.course);
    this.props.onSubmit(this.state.course);
  };
  render() {
    let { course, edit } = this.state;
    let { courseName, faculty, lectures } = course;
    return (
      <React.Fragment>
        <div className="container">
          <h5>{edit ? "Edit Course Detail" : "Enter Details of Course"}</h5>
          <div className="form-group">
            <label>Course Nmae</label>
            <input
              type="text"
              className="form-control"
              id="coursename"
              name="courseName"
              value={courseName}
              placeholder="Enter Course Name "
              onChange={this.handelChange}
            />
          </div>

          <div className="form-group">
            <label>Faculty</label>
            <input
              type="text"
              className="form-control"
              id="faculty"
              name="faculty"
              value={faculty}
              placeholder="Enter Name of Faculty "
              onChange={this.handelChange}
            />
          </div>
          <div className="form-group">
            <label>Lectures</label>
            <input
              type="number" 
              className="form-control"
              id="lectures"
              name="lectures"
              value={lectures}
              placeholder="Enter Number of Lectures "
              onChange={this.handelChange}
            />
          </div>

          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            {edit ? "Update Course" : "Add Course"}
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default CourseForm;
