import React, { Component } from "react";
import MarkForm from "./task5.MarksForm";
import StudentForm from "./task5.StudentForm";
class Task5HomeScreen extends Component {
  state = {
    students: [],
    view: 0,
    addMarkIndex: -1,
    editMarkIndex: -1,
  };

  showForm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };

  handleStudentList = () => {
    let s1 = { ...this.state };
    s1.view = 2;
    this.setState(s1);
  };

  handelSubmitStudent = (std) => {
    let s1 = { ...this.state };
    s1.students.push(std);
    s1.view = 2;
    this.setState(s1);
  };

  showMarkForm = (index) => {
    let s1 = { ...this.state };
    s1.addMarkIndex = index;
    s1.view = 3;
    this.setState(s1);
  };

  showEditMarkForm = (index) => {
    let s1 = { ...this.state };
    s1.editMarkIndex = index;
    s1.view = 3;
    this.setState(s1);
  };

  handelSubmitMark = (mrk) => {
    let s1 = { ...this.state };
    s1.editMarkIndex >= 0
      ? (s1.students[s1.editMarkIndex].mark[s1.editMarkIndex] = mrk)
      : s1.students[s1.addMarkIndex].mark.push(mrk);
    s1.view = 2;
    console.log(s1.students[s1.addMarkIndex].mark)
    s1.editMarkIndex = -1;
    this.setState(s1);
  };
  render() {
    let { students, view, editMarkIndex } = this.state;
    let student1 = { name: "", course: "", year: "", mark: [] };
    let mark1 = { math: 0, english: 0, computer: 0, science: 0 };
    return (
      <React.Fragment>
        <div className="container">
          <button className="btn btn-primary m-2" onClick={this.showForm}>
            New Students
          </button>

          <button
            className="btn btn-primary m-2"
            onClick={this.handleStudentList}
          >
            List Of Students
          </button>
          {view === 0 ? (
            <h4>Welcome to the Student Management System</h4>
          ) : view === 1 ? (
            <StudentForm
              student={student1}
              onSubmit={this.handelSubmitStudent}
            />
          ) : view === 2 ? (
            students.length === 0 ? (
              <h6>Ther are ZERO Students</h6>
            ) : (
              <>
                <div className="row border bg-dark text-white">
                  <div className="col-3">Name</div>
                  <div className="col-3">Course</div>
                  <div className="col-2">Year</div>
                  <div className="col-2">Total Mark</div>
                  <div className="col-2"></div>
                </div>
                {students.map((s1, index) => {
                  let { name, course, year, mark = [] } = s1;
                  let { math, english, computer, science } = mark;
                  return (
                    <div className="row border ">
                      <div className="col-3">{name}</div>
                      <div className="col-3">{course}</div>
                      <div className="col-2">{year}</div>
                      <div className="col-2">
                        {mark.length === 0
                          ? "No Data"
                          : <>{math + english + computer + science }</>}
                      </div>
                      <div className="col-2 text-center">
                        {" "}
                        {mark.length > 0 ? (
                          <button
                            className="btn btn-success btn-sm m-2"
                            onClick={() => this.showEditMarkForm(index)}
                          >
                            Edit{" "}
                          </button>
                        ) : (
                          <button
                            className="btn btn-success btn-sm m-2"
                            onClick={() => this.showMarkForm(index)}
                          >
                            Enter{" "}
                          </button>
                        )}
                      </div>
                    </div>
                  );
                })}
              </>
            )
          ) : (
            <MarkForm
              mark={
                editMarkIndex >= 0
                  ? students[editMarkIndex].mark[editMarkIndex]
                  : mark1
              }
              onSubmit={this.handelSubmitMark}
              edit={editMarkIndex >= 0}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default Task5HomeScreen;
