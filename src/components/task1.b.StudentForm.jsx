import React, { Component } from "react";
class StudentForm extends Component {
    state = {
        student: this.props.student,
      };

    handelChange = (e) => {
        let s1 = { ...this.state };
        s1.student[e.currentTarget.name] = e.currentTarget.value;
        this.setState(s1);
    };
    
    
      handelSubmit = (e) => {
        e.preventDefault();
        console.log("handel submit ", this.state.student);
        this.props.onSubmit(this.state.student);
      };

    render(){
        let {student,onSubmit}=this.props
        return(
<React.Fragment>
<div className="container">
          <h5>Enroll  Student in Course </h5>
          <div className="form-group">
            <label>Student ID</label>
            <input
              type="text"
              className="form-control"
              id="id"
              name="id"
              value={student.id}
              placeholder="Enter Student ID "
              onChange={this.handelChange}
            />
          </div>

          <div className="form-group">
            <label>Name of the Student</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={student.name}
              placeholder="Enter Student Name  "
              onChange={this.handelChange}
            />
          </div>

          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            Enroll Student
          </button>
          </div>
</React.Fragment>
        )
    }
}
export default StudentForm;
