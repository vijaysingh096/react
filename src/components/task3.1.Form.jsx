import React, { Component } from "react";
class Task3_1CourseForm extends Component {
  state = {
    course: this.props.course,
    edit: this.props.edit,
  };

  handelChange = (e) => {
    let s1 = { ...this.state };
    s1.course = e.currentTarget.value;
    this.setState(s1);
  };

  handelSubmit = (e) => {
    e.preventDefault();
    // console.log("handel submit ", this.state.course);
    this.props.onSubmit(this.state.course);
  };
  render() {
    let { course, edit } = this.state;

    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Add a Course</label>
            <input
              type="text"
              className="form-control"
              id="coursename"
              name="course"
              value={course}
              placeholder="Enter Course Name "
              onChange={this.handelChange}
            />
          </div>
          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            {edit ? "Update Course" : "Submit"}
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default Task3_1CourseForm;
