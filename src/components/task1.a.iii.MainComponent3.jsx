import React, { Component } from "react";
import SimpleForm3 from "./task1.a.iii.SimpleForm3";
class MainComponent3 extends Component {
  state = {
    persons: [
      { name: "Jack", age: 24 },
      { name: "Mary", age: 21 },
    ],
    view: 0, //0-show person table and button,1- show form
  };
  handlePerson = (Person) => {
    console.log("in handel person", Person);
    let s1 = { ...this.state };
    s1.persons.push(Person);
    s1.view = 0;
    this.setState(s1);
  };
  showSorm = () => {
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);
  };
  render() {
    let person = { name: "", age: "" };
    let { persons, view } = this.state;
    return (
      <React.Fragment>
        {view === 0 ? (
          <div className="container">
            {persons.map((p1) => {
              let { name, age } = p1;
              return (
                <div className="row border">
                  <div className="col-6 border">{name}</div>
                  <div className="col-6 border">{age}</div>
                </div>
              );
            })}
            <button
              className="btn btn-primary my-2"
              onClick={() => this.showSorm()}
            >
              Add New Person{" "}
            </button>
          </div>
        ) : (
          <SimpleForm3 person={person} onSubmit={this.handlePerson} />
        )}
      </React.Fragment>
    );
  }
}
export default MainComponent3;
