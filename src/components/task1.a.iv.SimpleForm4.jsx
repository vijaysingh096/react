import React, { Component } from "react";
class SimpleForm4 extends Component {
  state = {
    person: this.props.person,
    edit: this.props.edit,
  };

  handelChange = (e) => {
    console.log(e.currentTarget);
    let s1 = { ...this.state };

    s1.person[e.currentTarget.name] = e.currentTarget.value;
    this.setState(s1);
  };

  handelSubmit = (e) => {
    e.preventDefault();
    console.log("handel submit ", this.state.person);
    this.props.onSubmit(this.state.person);
  };

  render() {
    let { person, edit } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h5>{edit ? "Edit Detail" : "Enter Details of Person"}</h5>
          <div className="form-group">
            <label>Nmae</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={person.name}
              placeholder="Enter Name "
              onChange={this.handelChange}
            />
          </div>

          <div className="form-group">
            <label>Age</label>
            <input
              type="text"
              className="form-control"
              id="age"
              name="age"
              value={person.age}
              placeholder="Enter your age "
              onChange={this.handelChange}
            />
          </div>
          <button className="btn btn-primary my-2" onClick={this.handelSubmit}>
            {edit ? "Update" : "Submit"}
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default SimpleForm4;
