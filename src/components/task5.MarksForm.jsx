import React, { Component } from "react";
class MarkForm extends Component {
  state = {
    mark: this.props.mark,
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    s1.mark[e.currentTarget.name] = (e.currentTarget.value);
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state.mark);
  };
  render() {
    let { math, english, computer, science } = this.state.mark;
    return (
      <React.Fragment>
        <div className="container">
          <div className="form-group">
            <label>Math</label>
            <input
              type="number"
              className="form-control"
              id="math"
              name="math"
              value={math}
              placeholder="Enter mark of math "
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>English</label>
            <input
              type="number"
              className="form-control"
              id="english"
              name="english"
              value={english}
              placeholder="Enter mark of english "
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Computer</label>
            <input
              type="number"
              className="form-control"
              id="computer"
              name="computer"
              value={computer}
              placeholder="Enter mark of computer "
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Science</label>
            <input
              type="number"
              className="form-control"
              id="science"
              name="science"
              value={science}
              placeholder="Enter mark of science "
              onChange={this.handleChange}
            />
          </div>
          <button className="btn btn-primary m-2" onClick={this.handleSubmit}>
            Submit
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default MarkForm;
