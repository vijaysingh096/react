import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import SimpleForm from "./components/task1.a.i";
import MainComponent from "./components/task1.a.ii.MainComponent";
import MainComponent3 from "./components/task1.a.iii.MainComponent3";
import MainComponent4 from "./components/task1.a.iv.MainComponent4";
import CouresComponent from "./components/task1.b.CourseComponent";
import SimpleForm2 from "./components/task2.SimpleForm2";
import Task3_1MainCompo from "./components/task3.1.MainCompo";
import HomeScreen from "./components/task4.HomeScreen";
import Task5HomeScreen from "./components/task5.HomeScren";

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <SimpleForm /> */}
    {/* <MainComponent /> */}
    {/* <MainComponent3 /> */}
    {/* <MainComponent4 /> */}
    {/* <CouresComponent /> */}
    {/* <SimpleForm2 /> */}
    {/* <Task3_1MainCompo /> */}
    {/* <HomeScreen /> */}
    <Task5HomeScreen />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
